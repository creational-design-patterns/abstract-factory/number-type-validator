package factories.concrete_factories;

import factories.NumberTypeValidatorFactory;
import products.number_type_family.NumberTypeValidator;
import products.number_type_family.concrete_validators.ByteValidator;
import products.positive_number_type_family.PositiveNumberTypeValidator;
import products.positive_number_type_family.concrete_validators.PositiveByteValidator;

public class ByteValidatorFactory extends NumberTypeValidatorFactory {
    @Override
    public NumberTypeValidator getNumberTypeValidator() {
        ByteValidator byteValidator = new ByteValidator();

        System.out.println("ByteValidator has been successfully instantiated");

        return byteValidator;
    }

    @Override
    public PositiveNumberTypeValidator getPositiveNumberTypeValidator() {
        PositiveByteValidator positiveByteValidator = new PositiveByteValidator();

        System.out.println("PositiveByteValidator has been successfully instantiated");

        return positiveByteValidator;
    }
}
