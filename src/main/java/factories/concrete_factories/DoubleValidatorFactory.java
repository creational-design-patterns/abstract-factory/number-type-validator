package factories.concrete_factories;

import factories.NumberTypeValidatorFactory;
import products.number_type_family.NumberTypeValidator;
import products.number_type_family.concrete_validators.DoubleValidator;
import products.positive_number_type_family.PositiveNumberTypeValidator;
import products.positive_number_type_family.concrete_validators.PositiveDoubleValidator;

public class DoubleValidatorFactory extends NumberTypeValidatorFactory {
    @Override
    public NumberTypeValidator getNumberTypeValidator() {
        DoubleValidator doubleValidator = new DoubleValidator();

        System.out.println("DoubleValidator has been successfully instantiated");

        return doubleValidator;
    }

    @Override
    public PositiveNumberTypeValidator getPositiveNumberTypeValidator() {
        PositiveDoubleValidator positiveDoubleValidator = new PositiveDoubleValidator();

        System.out.println("PositiveDoubleValidator has been successfully instantiated");

        return positiveDoubleValidator;
    }
}