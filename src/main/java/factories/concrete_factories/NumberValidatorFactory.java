package factories.concrete_factories;

import factories.NumberTypeValidatorFactory;
import products.number_type_family.NumberTypeValidator;
import products.number_type_family.concrete_validators.NumberValidator;
import products.positive_number_type_family.PositiveNumberTypeValidator;
import products.positive_number_type_family.concrete_validators.PositiveNumberValidator;

public class NumberValidatorFactory extends NumberTypeValidatorFactory {

    @Override
    public NumberTypeValidator getNumberTypeValidator() {
        NumberValidator numberValidator = new NumberValidator();

        System.out.println("NumberValidator has been successfully instantiated");

        return numberValidator;
    }

    @Override
    public PositiveNumberTypeValidator getPositiveNumberTypeValidator() {
        PositiveNumberValidator positiveNumberValidator = new PositiveNumberValidator();

        System.out.println("PositiveNumberValidator has been successfully instantiated");

        return positiveNumberValidator;
    }
}
