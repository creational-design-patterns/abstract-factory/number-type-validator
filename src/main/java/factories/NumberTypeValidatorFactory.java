package factories;

import enums.NumberType;
import factories.concrete_factories.ByteValidatorFactory;
import factories.concrete_factories.DoubleValidatorFactory;
import factories.concrete_factories.NumberValidatorFactory;
import products.number_type_family.NumberTypeValidator;
import products.positive_number_type_family.PositiveNumberTypeValidator;


public abstract class NumberTypeValidatorFactory {
    public abstract NumberTypeValidator getNumberTypeValidator();
    public abstract PositiveNumberTypeValidator getPositiveNumberTypeValidator();


    public static NumberTypeValidatorFactory getNumberTypeValidatorFactory(NumberType numberType) {
        NumberTypeValidatorFactory numberTypeValidatorFactory;

        switch (numberType) {
            case BYTE -> {
                numberTypeValidatorFactory = new ByteValidatorFactory();
                System.out.printf("Factory implementation for a Byte type validation has been successfully instantiated%n");
            }
            case DOUBLE -> {
                numberTypeValidatorFactory = new DoubleValidatorFactory();
                System.out.printf("Factory implementation for a Double type validation has been successfully instantiated%n");
            }
            case null, default -> {
                numberTypeValidatorFactory = new NumberValidatorFactory();
                System.out.printf("Default factory implementation for a Number type validation has been successfully instantiated%n");
            }
        }

        return numberTypeValidatorFactory;
    }
}
