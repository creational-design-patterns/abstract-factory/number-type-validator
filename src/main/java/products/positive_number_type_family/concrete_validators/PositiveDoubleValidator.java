package products.positive_number_type_family.concrete_validators;

import products.positive_number_type_family.PositiveNumberTypeValidator;

import java.math.BigDecimal;

public class PositiveDoubleValidator implements PositiveNumberTypeValidator {
    private final BigDecimal MAX_VALUE = new BigDecimal(String.valueOf(Double.MAX_VALUE));
    private final BigDecimal MIN_VALUE = new BigDecimal(String.valueOf(0));

    @Override
    public void validate(Number number) {
        String value = String.valueOf(number.toString());

        try {
            BigDecimal valueAsBigDecimal = new BigDecimal(value);

            int scale = valueAsBigDecimal.scale();
            boolean isValid = MAX_VALUE.compareTo(valueAsBigDecimal) >= 0
                    && MIN_VALUE.compareTo(valueAsBigDecimal) < 0
                    && scale > 0;

            if (isValid) {
                System.out.printf("%s is a valid positive Double!%n", number);
            } else {
                System.out.printf("%s isn't a positive Double!%n", number);
            }

        } catch (NumberFormatException e) {
            System.out.printf("%s isn't a positive Double!%n", number);
        }
    }
}
