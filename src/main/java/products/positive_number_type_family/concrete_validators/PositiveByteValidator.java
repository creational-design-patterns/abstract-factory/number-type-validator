package products.positive_number_type_family.concrete_validators;

import products.positive_number_type_family.PositiveNumberTypeValidator;

import java.math.BigInteger;

public class PositiveByteValidator implements PositiveNumberTypeValidator {
    private final BigInteger MAX_VALUE = new BigInteger(String.valueOf(Byte.MAX_VALUE));
    private final BigInteger MIN_VALUE = new BigInteger(String.valueOf(0));

    @Override
    public void validate(Number number) {
        String value = String.valueOf(number.toString());

        try {
            BigInteger valueAsBigInteger = new BigInteger(value);

            boolean isValid = MAX_VALUE.compareTo(valueAsBigInteger) >= 0
                    && MIN_VALUE.compareTo(valueAsBigInteger) < 0;

            if (isValid) {
                System.out.printf("%s is a valid positive Byte!%n", number);
            } else {
                System.out.printf("%s isn't a positive Byte!%n", number);
            }

        } catch (NumberFormatException e) {
            System.out.printf("%s isn't a positive Byte!%n", number);
        }

    }
}
