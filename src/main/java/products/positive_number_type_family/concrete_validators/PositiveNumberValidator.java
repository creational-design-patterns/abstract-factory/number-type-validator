package products.positive_number_type_family.concrete_validators;

import products.positive_number_type_family.PositiveNumberTypeValidator;

import java.math.BigDecimal;

public class PositiveNumberValidator implements PositiveNumberTypeValidator {
    @Override
    public void validate(Number number) {
        String value = String.valueOf(number.toString());

        BigDecimal valueAsBigDecimal = new BigDecimal(value);

        if(valueAsBigDecimal.compareTo(BigDecimal.valueOf(0)) > 0){
            System.out.printf("%s is a valid positive Number!%n", number);
        } else {
            System.out.printf("%s isn't a valid positive Number!%n", number);
        }


    }
}
