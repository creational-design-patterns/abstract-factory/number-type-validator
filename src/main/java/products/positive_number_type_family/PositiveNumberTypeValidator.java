package products.positive_number_type_family;

public interface PositiveNumberTypeValidator {
    void validate(Number number);
}
