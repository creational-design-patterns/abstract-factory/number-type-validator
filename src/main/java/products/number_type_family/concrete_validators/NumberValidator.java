package products.number_type_family.concrete_validators;

import products.number_type_family.NumberTypeValidator;

public class NumberValidator implements NumberTypeValidator {
    @Override
    public void validate(Number number) {
        System.out.printf("%s is a valid Number!%n", number);
    }
}
