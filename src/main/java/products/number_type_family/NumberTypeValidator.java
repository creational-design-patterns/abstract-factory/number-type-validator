package products.number_type_family;

public interface NumberTypeValidator {
    void validate(Number number);
}
